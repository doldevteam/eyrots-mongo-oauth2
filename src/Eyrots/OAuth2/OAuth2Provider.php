<?php
namespace Eyrots\OAuth2;

class OAuth2Provider
{
  public static $container = null;
  public static function configurationFilters(Container $container)
  {
    ConfigReducer::addPostFilter(function($config) use($container){
      $models = (array)($config['mongodb.models'] ?? []);
      $models[] = __DIR__ . '/Model';
      $config['mongodb.models'] = $models;
      return $config;
    });
  }
}