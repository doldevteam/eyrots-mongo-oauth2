<?php
namespace Eyrots\OAuth2\Repository;

use \League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use \League\OAuth2\Server\Entities\ClientEntityInterface;
use \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;


use \OAuth2ServerExamples\Entities\AccessTokenEntity;

class AccessToken implements AccessTokenRepositoryInterface
{
  public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
  {
    
  }
  public function revokeAccessToken($tokenId)
  {
    // Some logic here to revoke the access token
  }
  public function isAccessTokenRevoked($tokenId)
  {
    return false;
  }
  public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
  {
    $accessToken = new AccessTokenEntity();
    $accessToken->setClient($clientEntity);
    foreach ($scopes as $scope) {
      $accessToken->addScope($scope);
    }
    $accessToken->setUserIdentifier($userIdentifier);
    return $accessToken;
  }
}