<?php
namespace Eyrots\OAuth2\Repository;

use \League\OAuth2\Server\Repositories\UserRepositoryInterface;
use \League\OAuth2\Server\Entities\ClientEntityInterface;
use \League\OAuth2\Server\Entities\UserEntityInterface;

class User implements UserRepositoryInterface
{
  /**
   * Get a user entity.
   *
   * @param string                $username
   * @param string                $password
   * @param string                $grantType    The grant type used
   * @param ClientEntityInterface $clientEntity
   *
   * @return UserEntityInterface
   */
  public function getUserEntityByUserCredentials($username, $password, $grantType, ClientEntityInterface $clientEntity)
  {
    
  }
}