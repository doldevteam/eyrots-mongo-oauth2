<?php
namespace Eyrots\OAuth2\Model;

use \Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use \MongoDB\BSON\ObjectId;

/**
 * @ODM\Document
 */
class RefreshToken implements RefreshTokenEntityInterface
{
  /**
   * @var ObjectId
   * 
   * @ODM\Id
   */
  protected $id;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $identifier;
  /**
   * @var DateTime
   * 
   * @ODM\Field(type="date")
   */
  protected $expiryDateTime;
  /**
   * @var AccessTokenEntityInterface
   */
  protected $accessToken;

  /**
   * @return mixed
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }
  /**
   * @param mixed $identifier
   */
  public function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
  }
  /**
   * Get the token's expiry date time.
   *
   * @return DateTime
   */
  public function getExpiryDateTime()
  {
    return $this->expiryDateTime;
  }
  /**
   * Set the date time when the token expires.
   *
   * @param DateTime $dateTime
   */
  public function setExpiryDateTime(DateTime $dateTime)
  {
    $this->expiryDateTime = $dateTime;
  }
  /**
   * Set access token
   * 
   * @param AccesTokenEntityInterface $accessToken
   */
  public function setAccessToken(AccessTokenEntityInterface $accessToken)
  {
      $this->accessToken = $accessToken;
  }
  /**
   * Get access token
   * 
   * @return AccessTokenEntityInterface
   */
  public function getAccessToken()
  {
      return $this->accessToken;
  }
}