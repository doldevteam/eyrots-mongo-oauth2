<?php
namespace Eyrots\OAuth2\Model;

use \Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \League\OAuth2\Server\Entities\ClientEntityInterface;
use \MongoDB\BSON\ObjectId;

/**
 * @ODM\Document
 */
class Client implements ClientEntityInterface
{
  /**
   * @var ObjectId
   * 
   * @ODM\Id
   */
  protected $id;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $identifier;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $name;
  /**
   * @var string[]
   * 
   * @ODM\Field(type="collection")
   */
  protected $redirectUri;

  /**
   * Get entity Id
   *
   * @return MongoId
   */
  public function getId()
  {
    return $this->id;
  }
  /**
   * @return mixed
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }
  /**
   * @param mixed $identifier
   */
  public function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
  }
  /**
   * Get the client's name.
   *
   * @return string
   * @codeCoverageIgnore
   */
  public function getName()
  {
    return $this->name;
  }
  /**
   * Set the client's name.
   *
   * @param string $name
   * @return void
   */
  public function setName($name)
  {
    $this->name = $name;
  }
  /**
   * Returns the registered redirect URI as an
   * indexed array of redirect URIs.
   *
   * @return string[]
   */
  public function getRedirectUri()
  {
    return $this->redirectUri;
  }
  /**
   * Sets the redirect URIs array
   *
   * @param string[] $uris
   * @return void
   */
  public function setRedirectUri($uris)
  {
    $this->redirectUri = $uris;
  }
  /**
   * Add a redirect URI
   *
   * @param string $uri
   * @return void
   */
  public function addRedirectUri($uri)
  {
    if(!\in_array($uri, $this->redirectUri))
    {
      $this->redirectUri[] = $uri;
    }
  }
  /**
   * Removes a redirect URI
   *
   * @param string $uri
   * @return void
   */
  public function removeRedirectUri($uri)
  {
    if(false !== ($index = array_search($uri, $this->redirectUri)))
    {
      \array_splice($this->redirectUri, $index, 1);
    }
  }
}