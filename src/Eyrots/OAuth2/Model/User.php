<?php
namespace Eyrots\OAuth2\Model;

use \Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \League\OAuth2\Server\Entities\UserEntityInterface;
use \MongoDB\BSON\ObjectId;

/**
 * @ODM\Document
 */
class User implements UserEntityInterface
{
  /**
   * @var ObjectId
   * 
   * @ODM\Id
   */
  protected $id;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $firstName;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $lastName;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $username;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $password;

  /**
   * Get user id
   *
   * @return ObjectId
   */
  public function getId()
  {
    return $this->id;
  }
  /**
   * Set user id
   *
   * @param ObjectId $id
   * @return void
   */
  public function setId(ObjectId $id)
  {
    $this->id = $id;
  }
  /**
   * Get user first name
   *
   * @return string
   */
  public function getFirstName()
  {
    return $this->firstName;
  }
  /**
   * Set user first name
   *
   * @param string $firstName
   * @return void
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
  }
  /**
   * Get user last name
   *
   * @return string
   */
  public function getLastName()
  {
    return $this->lastName;
  }
  /**
   * Set user last name
   *
   * @param string $lastName
   * @return void
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
  }
  /**
   * Get user username
   *
   * @return string
   */
  public function getUsername()
  {
    return $this->username;
  }
  /**
   * Set user username
   *
   * @param string $username
   * @return void
   */
  public function setUsername($username)
  {
    $this->username = $username;
  }
  /**
   * Get user password (returns null)
   *
   * @return null
   */
  public function getPassword()
  {
    return null;
  }
  /**
   * Set user password
   *
   * @param string $password
   * @return void
   */
  public function setPassword($password)
  {
    $this->password = \password_hash($password, \PASSWORD_BCRYPT);
  }
}