<?php
namespace Eyrots\OAuth2\Model;

use \Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \League\OAuth2\Server\Entities\ScopeEntityInterface;
use \League\OAuth2\Server\Entities\Traits\ScopeTrait;
use \MongoDB\BSON\ObjectId;

/**
 * @ODM\Document
 */
class Scope implements ScopeEntityInterface
{
  use ScopeTrait;

  /**
   * @var ObjectId
   * 
   * @ODM\Id
   */
  protected $id;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $identifier;

  /**
   * @return mixed
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }
  /**
   * @param mixed $identifier
   */
  public function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
  }
}