<?php
namespace Eyrots\OAuth2\Model;

use \Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use \League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use \MongoDB\BSON\ObjectId;

/**
 * @ODM\Document
 */
class AccessToken implements AccessTokenEntityInterface
{
  use AccessTokenTrait;

  /**
   * @var ObjectId
   * 
   * @ODM\Id
   */
  protected $id;
  /**
   * @var string
   * 
   * @ODM\Field(type="string")
   */
  protected $identifier;
  /**
   * @var ScopeEntityInterface[]
   * 
   * @ODM\ReferenceMany(targetDocument=Scope::class)
   */
  protected $scopes = [];
  /**
   * @var DateTime
   * 
   * @ODM\Field(type="date")
   */
  protected $expiryDateTime;
  /**
   * @var string|int|null
   * 
   * @ODM\Field(type="string")
   */
  protected $userIdentifier;
  /**
   * @var ClientEntityInterface
   * 
   * @ODM\ReferenceOne(targetDocument=Client::class)
   */
  protected $client;

  /**
   * @return mixed
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }
  /**
   * @param mixed $identifier
   */
  public function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
  }
  /**
   * Associate a scope with the token.
   *
   * @param ScopeEntityInterface $scope
   */
  public function addScope(ScopeEntityInterface $scope)
  {
    $this->scopes[$scope->getIdentifier()] = $scope;
  }
  /**
   * Return an array of scopes associated with the token.
   *
   * @return ScopeEntityInterface[]
   */
  public function getScopes()
  {
    return array_values($this->scopes);
  }
  /**
   * Get the token's expiry date time.
   *
   * @return DateTime
   */
  public function getExpiryDateTime()
  {
    return $this->expiryDateTime;
  }
  /**
   * Set the date time when the token expires.
   *
   * @param DateTime $dateTime
   */
  public function setExpiryDateTime(DateTime $dateTime)
  {
    $this->expiryDateTime = $dateTime;
  }
  /**
   * Set the identifier of the user associated with the token.
   *
   * @param string|int|null $identifier The identifier of the user
   */
  public function setUserIdentifier($identifier)
  {
    $this->userIdentifier = $identifier;
  }
  /**
   * Get the token user's identifier.
   *
   * @return string|int|null
   */
  public function getUserIdentifier()
  {
    return $this->userIdentifier;
  }
  /**
   * Get the client that the token was issued to.
   *
   * @return ClientEntityInterface
   */
  public function getClient()
  {
    return $this->client;
  }
  /**
   * Set the client that the token was issued to.
   *
   * @param ClientEntityInterface $client
   */
  public function setClient(ClientEntityInterface $client)
  {
    $this->client = $client;
  }
}